var array = [];

function input_func() {
  n = document.getElementById("input-n").value * 1;
  array.push(n);
  document.getElementById(
    "js6-array"
  ).innerHTML = `<div>Mảng đang xét: [${array}]</div>`;
}

function bai_1() {
  var sum = 0;
  for (var i = 0; i < array.length; i++) {
    if (array[i] > 0) {
      sum += array[i];
    }
  }
  document.getElementById("b1-sum").innerText = sum;
}

function bai_2() {
  var count = 0;
  for (var i = 0; i < array.length; i++) {
    if (array[i] > 0) {
      count++;
    }
  }
  document.getElementById("b2-count-pos").innerText = count + " số dương";
}

function bai_3() {
  var min = array[0];
  for (var i = 0; i < array.length; i++) {
    if (array[i] < min) {
      min = array[i];
    }
  }
  document.getElementById("b3-min").innerText = "Số nhỏ nhất: " + min;
}

function bai_4() {
  for (var i = 0; i < array.length; i++) {
    if (array[i] > 0) {
      min = array[i];
      for (var j = i; j < array.length; j++) {
        if (array[j] < min && array[j] > 0) {
          min = array[j];
        }
      }
      document.getElementById("b4-min-pos").innerText =
        "Số dương nhỏ nhất: " + min;
      return;
    }
  }
  document.getElementById("b4-min-pos").innerText = "Không có số dương";
}

function bai_5() {
  for (var i = 0; i < array.length; i++) {
    if (array[i] % 2 == 0) {
      last_even = array[i];
      for (var j = i; j < array.length; j++) {
        if (array[j] % 2 == 0) {
          last_even = array[j];
        }
      }
      document.getElementById("b5-last-even").innerText =
        "Số chẵn cuối cùng: " + last_even;
      return;
    }
  }
  document.getElementById("b5-last-even").innerText = "Không có số chẵn";
}

function bai_6() {
  var array_copy = [...array];
  var a = document.getElementById("b6-a").value * 1;
  var b = document.getElementById("b6-b").value * 1;
  var c;
  c = array_copy[a];
  array_copy[a] = array_copy[b];
  array_copy[b] = c;
  document.getElementById(
    "b6-swap"
  ).innerHTML = `<div>Mảng sau khi thay đổi : ${array_copy}</div>`;
}

function bai_7() {
  var array_copy = [...array];
  for (var i = 0; i < array_copy.length; i++) {
    for (var j = i; j < array_copy.length; j++) {
      if (array_copy[i] > array_copy[j]) {
        var c = array_copy[i];
        array_copy[i] = array_copy[j];
        array_copy[j] = c;
      }
    }
  }
  document.getElementById(
    "b7-sort"
  ).innerHTML = `<div>Mảng sau khi sắp xếp : ${array_copy}</div>`;
}

function is_prime(num) {
  for (var i = 2, s = Math.sqrt(num); i <= s; i++)
    if (num % i == 0) return false;
  return num > 1;
}

function bai_8() {
  for (var i = 0; i < array.length; i++) {
    if (is_prime(array[i])) {
      document.getElementById(
        "b8-prime"
      ).innerHTML = `<div>Số nguyên tố: ${array[i]}</div>`;
      return;
    } else {
      document.getElementById(
        "b8-prime"
      ).innerHTML = `<div>Không tìm thấy số nguyên tố</div>`;
    }
  }
}

function bai_9() {
  var count = 0;
  for (var i = 0; i < array.length; i++) {
    if (Number.isInteger(array[i])) {
      count++;
    }
  }
  document.getElementById(
    "b9-count-int"
  ).innerHTML = `<div>Có ${count} số nguyên tố trong mảng</div>`;
}

function bai_10() {
  var pos = 0;
  var neg = 0;
  for (var i = 0; i < array.length; i++) {
    if (array[i] > 0) {
      pos++;
    } else if (array[i] < 0) {
      neg++;
    }
  }
  if (pos > neg) {
    document.getElementById("b10-compare").innerHTML =
      "Số dương nhiều hơn số âm";
  } else if (pos == neg) {
    document.getElementById("b10-compare").innerHTML = "Âm dương cân bằng";
  } else {
    document.getElementById("b10-compare").innerHTML =
      "Số âm nhiều hơn số dương";
  }
}
